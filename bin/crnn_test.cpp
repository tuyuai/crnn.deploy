/*******************************************************************************
 *  Project: TuYuOCR
 *  Purpose: OpenSource OCR Engine
 *  Author: TuYuOCR contributors
 *******************************************************************************
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2019 TuYuOCR contributors
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *******************************************************************************/

#include "crnn/crnn.hpp"
#include "spdlog/spdlog.h"

using namespace tuyu;

int main(int argc, char* argv[]) {
  std::string model_path = "../models/crnn.onnx";
  std::string json_file = "../models/alphabet.json";
  CRNN crnn(model_path, json_file);

  std::string input_file = "../../images/1.png";
  cv::Mat image = cv::imread(input_file);
  std::string res = crnn.Predict(image);
  SPDLOG_INFO("recognize result is {}\n", res);
  return 0;
}