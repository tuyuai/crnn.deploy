<img src="docs/logo.png" align="right" style="padding-left: 20px" height="40px" />

# TuYuOCR

*TuYuOCR* is an open-source, cross-platform, multi-language(now, chinese + english) support OCR engine based [CRNN](https://arxiv.org/abs/1507.05717) method.
this repo contains the Inference phase code, Training code is base on the deep learning library [PyTorch](https://github.com/pytorch/pytorch), and Inference code is based on
c++, [ONNX](https://github.com/onnx/onnx), [onnxruntime](https://github.com/microsoft/onnxruntime) and [opencv](https://opencv.org/), so it can be cross-platform, now is test on Linux and Mac.


# How to use TuYuOCR

## this [repo](https://gitee.com/tuyuai/crnn.deploy) constains the Inference code, for training please visit https://gitee.com/tuyuai/crnn.train

### Prerequsts
* opencv > 4.0
* onnxruntime = 0.5.0
* cmake > 3.8
  
### Build From Source
1. Get the Inference code
   ```bash
   $ git clone https://gitee.com/tuyuai/crnn.deploy.git
   ```
2. Build 
   ```bash
   $ cd crnn.deploy
   $ mkdir build && cd build
   $ cmake ..
   $ make
   ```  
3. Download pretrained models [models.zip](https://pan.baidu.com/s/1lnXtqwXCNdh4s6aTvXV2Sw)  密码:2jmd
4. Run the test 
   ```bash
   $ cd build
   $ ./crnn_test
   ```
   
   
### Training
please visit https://gitee.com/tuyuai/crnn.train

# License
[MIT License](LICENSE)

# Star
If this repo is useful for you, you can star or fork it.