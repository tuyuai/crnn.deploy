/*******************************************************************************
 *  Project: TuYuOCR
 *  Purpose: OpenSource OCR Engine
 *  Author: TuYuOCR contributors
 *******************************************************************************
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2019 TuYuOCR contributors
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *******************************************************************************/

#include "crnn/decode.hpp"
namespace tuyu {
std::vector<int> GreedyDecode(const std::vector<int> &preds) {
  std::vector<int> res;
  int preds_size = preds.size();
  for (int i = 0; i < preds_size; i++) {
    if (preds[i] != 0 && !(i > 0 && preds[i - 1] == preds[i])) {
      res.push_back(preds[i]);
    }
  }
  return res;
}

}  // namespace tuyu