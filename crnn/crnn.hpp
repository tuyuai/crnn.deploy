/*******************************************************************************
 *  Project: TuYuOCR
 *  Purpose: OpenSource OCR Engine
 *  Author: TuYuOCR contributors
 *******************************************************************************
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2019 TuYuOCR contributors
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *deal in the Software without restriction, including without limitation the
 *rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *IN THE SOFTWARE.
 *******************************************************************************/

#ifndef CRNN_H
#define CRNN_H
#include <opencv2/opencv.hpp>
#include <string>
#include "crnn/decode.hpp"
#include "json.hpp"
#include "onnxruntime_cxx_api.h"

using json = nlohmann::json;
namespace tuyu {
class CRNN {
 public:
  CRNN(const std::string& model_name, const std::string& json_filename);
  ~CRNN();

  std::string Predict(const cv::Mat& image);

 private:
  void InitModel(const std::string& model_name,
                 const std::string& json_filename);
  void PreprocessImage(const cv::Mat& image, cv::Mat& out);

  json json_alphabet;
  Ort::Env env_{nullptr};
  Ort::Session session_{nullptr};
};
}  // namespace tuyu
#endif  // CRNN_H
